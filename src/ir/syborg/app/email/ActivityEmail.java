package ir.syborg.app.email;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;


public class ActivityEmail extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        final EditText edtTo = (EditText) findViewById(R.id.edtTo);
        final EditText edtSub = (EditText) findViewById(R.id.edtSub);
        final EditText edtmass = (EditText) findViewById(R.id.edtMas);
        Button btnCom = (Button) findViewById(R.id.btnCom);

        btnCom.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                String[] strTo = { edtTo.getText().toString() };

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("message/rfc822");
                intent.putExtra(intent.EXTRA_EMAIL, strTo);
                intent.putExtra(intent.EXTRA_SUBJECT, edtSub.getText().toString());
                intent.putExtra(intent.EXTRA_TEXT, edtmass.getText().toString());
                startActivity(intent.createChooser(intent, "لطفا برنامه ارسال را انتخاب کنید"));
            }
        });

    }
}